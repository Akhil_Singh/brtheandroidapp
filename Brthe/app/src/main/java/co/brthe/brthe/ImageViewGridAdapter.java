package co.brthe.brthe;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;

import co.brthe.brthe.AnotMenu;
import co.brthe.brthe.R;

/**
 * Created by akhil on 13/3/15.
 */
public class ImageViewGridAdapter extends BaseAdapter {
    private Context mContext;

    private String[] filenames;

    // Gets the context so it can be used later
    public ImageViewGridAdapter(Context c, String[] FileNames) {
        mContext = c;
        filenames = FileNames;
    }

    // Total number of things contained within the adapter
    public int getCount() {
        return filenames.length;
    }

    // Require for structure, not really used in my code.
    public Object getItem(int position) {
        return null;
    }

    // Require for structure, not really used in my code. Can
    // be used to get the id of an item in the adapter for
    // manual control.
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position,
                        View convertView, ViewGroup parent) {
        ImageButton btn;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            btn = new ImageButton(mContext);
            btn.setScaleType(ImageView.ScaleType.FIT_CENTER);
            btn.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 200));
            btn.setPadding(10, 10, 10, 10);
        }
        else {
            btn = (ImageButton) convertView;
        }
        btn.setImageResource(R.drawable.pizzahut);
        btn.setBackgroundColor(0x00FFFFFF);
        btn.setId(position);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in;
                in = new Intent(mContext, AnotMenu.class);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                in.putExtra("ResName",filenames[position]);
                mContext.startActivity(in);

            }
        });

        return btn;
    }
}