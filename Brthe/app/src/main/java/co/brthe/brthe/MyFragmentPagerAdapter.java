package co.brthe.brthe;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


/**
 * Created by akhil on 11/3/15.
 */
public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

    // Holds tab titles
    private Context context;
    private  String[] singlesArray, combosArray;

    public MyFragmentPagerAdapter(FragmentManager fm, Context context, String[] SinglesArray, String[] CombosArray) {
        super(fm);
        this.context = context;
        this.singlesArray = SinglesArray;
        this.combosArray = CombosArray;
    }

    @Override
    public int getCount() {
        return 2;
    }

    // Return the correct Fragment based on index
    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            Fragment singles = new SelectSinglesFragment();
            Bundle args = new Bundle();
            args.putStringArray("singlesStringArray", this.singlesArray);
            singles.setArguments(args);
            return singles;
        } else if(position == 1) {
            Fragment combos = new SelectComboFragment();
            Bundle args = new Bundle();
            args.putStringArray("comboStringArray", this.combosArray);
            combos.setArguments(args);
            return combos;
        }

        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if(position == 0){
            return "Singles";
        }

        if( position == 1){
            return "Combos";
        }
        return  "Test";
    }
}

