package co.brthe.brthe.navbar;

/**
 * Created by akhil on 13/3/15.
 */
public class NavDrawerItem {

    private String title;
    private int icon;
    private String count = "0";
    private String headerTitle;
    // boolean to set visiblity of the counter
    private boolean isCounterVisible = false;
    private  boolean isLocation = false;
    private boolean isHeader = false;

    public NavDrawerItem(){}

    public NavDrawerItem(String title, int icon){
        this.title = title;
        this.icon = icon;
    }

    public NavDrawerItem(String title, int icon, boolean isCounterVisible, String count){
        this.title = title;
        this.icon = icon;
        this.isCounterVisible = isCounterVisible;
        this.count = count;
    }
    public NavDrawerItem(String headerTitle, boolean isHeader) {
        this(null, 0);
        this.headerTitle = headerTitle;
        this.isHeader  = isHeader;
    }

    public NavDrawerItem(Boolean isLocation){
        this(null, 0);
        this.isLocation  = isLocation;
    }

    public String getTitle(){
        return this.title;
    }

    public int getIcon(){
        return this.icon;
    }

    public String getCount(){
        return this.count;
    }

    public boolean getCounterVisibility(){
        return this.isCounterVisible;
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }


    public void setTitle(String title){
        this.title = title;
    }

    public void setIcon(int icon){
        this.icon = icon;
    }

    public void setCount(String count){
        this.count = count;
    }

    public void setCounterVisibility(boolean isCounterVisible){
        this.isCounterVisible = isCounterVisible;
    }

    public boolean isLocation() {
        return isLocation;
    }

    public boolean isHeader() {
        return isHeader;
    }
}