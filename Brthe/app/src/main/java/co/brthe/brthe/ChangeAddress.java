package co.brthe.brthe;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Spinner;

import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by akhil on 13/3/15.
 */
public class ChangeAddress extends Activity {


    private EditText mAddressEditText;
    private Spinner Spinner;
    private Button mSaveChanges;
    private Button mCancel;
    private AutoCompleteTextView actv;


    private String mAddress;
    private String mSpinner;
    private String mActv;
    private Filter filter;
    ArrayList<String> suggestion = new ArrayList<String>();;
    String[] locationSuges;

    ArrayAdapter<String> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_change_address);

        mAddressEditText = (EditText) findViewById(R.id.etAddressCA);
        actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);

        mSaveChanges = (Button) findViewById(R.id.saveChanges);
        mCancel = (Button) findViewById(R.id.cancel);


        ParseUser user = ParseUser.getCurrentUser();
        mAddressEditText.setText(user.get("address").toString());

        actv.setText((CharSequence) user.get("location"));


        if(suggestion == null){
            locationSuges = new String[0];

        }
        else {
            locationSuges = new String[suggestion.size()];
            locationSuges = suggestion.toArray(locationSuges);

        }


        filter = new Filter() {
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                Log.i("Filter",
                        "Filter:" + constraint + " thread: " + Thread.currentThread());
                //if (constraint != null && constraint.length() > 10) {
                new JSONLoadTask().execute("https://www.foodpanda.in/location-suggestions-ajax?cityId=17&area=" + URLEncoder.encode(actv.getText().toString()));

                return null;
            }
        };

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,suggestion) {
            public android.widget.Filter getFilter() {
                return filter;
            }
        };


        //if(actv.getText().toString() != "")
        //new JSONLoadTask().execute("https://www.foodpanda.in/location-suggestions-ajax?cityId=17&area=" + actv.getText().toString());

        /*
        if(suggestion == null){
            locationSuges = new String[0];

        }
        else {
            locationSuges = new String[suggestion.size()];
            locationSuges = suggestion.toArray(locationSuges);

        }*/
        actv.setThreshold(1);



        actv.setAdapter(adapter);
        adapter.setNotifyOnChange(true);


        actv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Log.isLoggable("2", count);
                //Log.d("below2", s.toString());
                //new JSONLoadTask().execute("https://www.foodpanda.in/location-suggestions-ajax?cityId=17&area=" + URLEncoder.encode(actv.getText().toString()));
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Log.d("1", "1");
                //new JSONLoadTask().execute("https://www.foodpanda.in/location-suggestions-ajax?cityId=17&area=" + URLEncoder.encode(actv.getText().toString()));



            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("3", "3");
                Log.d("below2", s.toString());
                new JSONLoadTask().execute("https://www.foodpanda.in/location-suggestions-ajax?cityId=17&area=" + URLEncoder.encode(actv.getText().toString()));



            }
        });



        mSaveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                mAddress = mAddressEditText.getText().toString();
                mActv = actv.getText().toString();


                ParseUser user = ParseUser.getCurrentUser();
                user.put("location", mActv);
                user.put("address", mAddress);

                user.saveInBackground(new SaveCallback() {

                    @Override
                    public void done(com.parse.ParseException e) {
                        //setProgressBarIndeterminateVisibility(false);

                        if (e == null) {
                            //Success!
                            Intent in;
                            in = new Intent(ChangeAddress.this, MainActivity.class);
                            startActivity(in);

                        } else {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });


        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in;
                in = new Intent(ChangeAddress.this, MainActivity.class);
                startActivity(in);


            }
        });




    }


    public class JSONLoadTask extends AsyncTask<String, Void, JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //your code
        }
        @Override
        protected JSONObject doInBackground(String... params) {
            JSONParser jParser = new JSONParser();
            JSONObject object = jParser.getJsonObject(params[0]);
            return object;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            JSONArray jArray = null;

            try {
                jArray = result.getJSONArray("data");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            try {
                int length = jArray.length();
                if(length > 5){
                    length = 5;
                }
                if(length != 0) {
                    int i;
                    suggestion = new ArrayList<String>();

                    for (i = 0; i < length; i++) {
                        suggestion.add(jArray.getJSONObject(i).getString("value"));
                    }
                }

                String[] locationSuges;

                if(suggestion == null){
                    locationSuges = new String[0];

                }
                else {
                    locationSuges = new String[suggestion.size()];
                    locationSuges = suggestion.toArray(locationSuges);

                }

                adapter.clear();
                int i = 0;
                if (locationSuges != null){
                    Log.d("I'm here", locationSuges.toString());

                    for (String st : locationSuges) {

                        adapter.insert(st, i);
                        Log.d("string", st);
                        i = i + 1;
                        if(i == 5){
                            break;
                        }
                    }
                }
                Log.d("changes", "here");
                // adapter[0] = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,locationSuges);
                adapter.notifyDataSetChanged();
                actv.showDropDown();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            //get the values from result here
        }

    }

}
