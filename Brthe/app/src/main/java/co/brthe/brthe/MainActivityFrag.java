package co.brthe.brthe;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.parse.ParseUser;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class MainActivityFrag extends Fragment {


    ImageButton item1 = null;
    ImageButton item2 = null;
    ImageButton item3 = null;
    ImageButton item4 = null;
    private Boolean changeAddressFlag = false;
    private TextView mGreetingsEditText;
    private Spinner spinner;
    private String mAddress;
    private String mSpinner;
    private GridView imageGridView;


    public String[] filenames = {
            "name1",
            "name2",
            "name3",
            "name4"
    };

    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
    List<String> stringArray = null;


    String url = "https://www.foodpanda.in/location-suggestions-ajax?cityId=17&area=gac";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_main_frag, container, false);

    //////////////////////////////////////////////////////code
        //String url = "https://www.foodpanda.in/location-suggestions-ajax?cityId=17&area=gac";
        //JSONParser jParser = new JSONParser();


        mGreetingsEditText = (TextView) rootView.findViewById(R.id.greetings);
        mGreetingsEditText.setText("Welcome " + ParseUser.getCurrentUser().getUsername() );
        imageGridView = (GridView) rootView.findViewById(R.id.gridviewimg);

        ImageViewGridAdapter theAdapter =  new ImageViewGridAdapter(getActivity().getBaseContext(), filenames);
        imageGridView.setAdapter(theAdapter);

        ///////////////MAIN LINE ///////////////////////////////////

       // new JSONLoadTask().execute(url);
        return rootView;
    }



}