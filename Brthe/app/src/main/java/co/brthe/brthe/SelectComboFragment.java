package co.brthe.brthe;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import co.brthe.brthe.common.view.HorizontalListView;

/**
 * Created by akhil on 10/3/15.
 */
public class SelectComboFragment extends Fragment{


    private String[] displayStringArray;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        displayStringArray = getArguments().getStringArray("comboStringArray");
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_combo_menu_display, container, false);

        final HorizontalListView listview1 = (HorizontalListView) view.findViewById(R.id.horizontalListView1);
        final HorizontalListView listview2 = (HorizontalListView) view.findViewById(R.id.horizontalListView2);

        ImageButton next1 = (ImageButton) view.findViewById(R.id.next1);
        ImageButton prev1 = (ImageButton) view.findViewById(R.id.prev1);

        ImageButton next2 = (ImageButton) view.findViewById(R.id.next2);
        ImageButton prev2 = (ImageButton) view.findViewById(R.id.prev2);

        next1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listview1.scrollTo(110);
            }
        });
        prev1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listview1.scrollTo(-110);
            }
        });

        next2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listview2.scrollTo(110);
            }
        });
        prev2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listview2.scrollTo(-110);
            }
        });

        listview1.setAdapter(mAdapter1);
        listview2.setAdapter(mAdapter2);

        return view;

    }



    private BaseAdapter mAdapter1 = new BaseAdapter() {

        int selectedPosition = 0;

        @Override
        public int getCount() {
            return displayStringArray.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View retval = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_2, null);
            TextView title = (TextView) retval.findViewById(R.id.title);
            title.setText(displayStringArray[position]);
            RadioButton radiobutton = (RadioButton) retval.findViewById(R.id.radioButtonCombo);
            radiobutton.setChecked(position == selectedPosition);
            radiobutton.setTag(position);
            radiobutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPosition = (Integer)view.getTag();
                    notifyDataSetChanged();
                    Toast.makeText(getActivity(), displayStringArray[position].toString() + "is Selected", Toast.LENGTH_SHORT).show();
                }
            });
            return retval;
        }

    };


    private BaseAdapter mAdapter2 = new BaseAdapter() {

        int selectedPosition = 0;

        @Override
        public int getCount() {
            return displayStringArray.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View retval = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_2, null);
            TextView title = (TextView) retval.findViewById(R.id.title);
            title.setText(displayStringArray[position]);
            RadioButton radiobutton = (RadioButton) retval.findViewById(R.id.radioButtonCombo);
            radiobutton.setChecked(position == selectedPosition);
            radiobutton.setTag(position);
            radiobutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPosition = (Integer)view.getTag();
                    notifyDataSetChanged();
                    Toast.makeText(getActivity(), displayStringArray[position].toString() + "is Selected", Toast.LENGTH_SHORT).show();
                }
            });
            return retval;
        }

    };
}


