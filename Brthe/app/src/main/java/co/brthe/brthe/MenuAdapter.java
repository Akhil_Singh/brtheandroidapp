package co.brthe.brthe;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by akhil on 11/3/15.
 */
public class MenuAdapter extends ArrayAdapter<String>  {

    LayoutInflater mInflater;
    String[] nameArray;
    int selectedPosition = 0;

    public MenuAdapter(Context context, String[] values){
        super(context, R.layout.row_layout, values);
        nameArray = values;
        mInflater = LayoutInflater.from(context);

           }
    // Override getView which is responsible for creating the rows for our list
    // position represents the index we are in for the array.

    // convertView is a reference to the previous view that is available for reuse. As
    // the user scrolls the information is populated as needed to conserve memory.

    // A ViewGroup are invisible containers that hold a bunch of views and
    // define their layout properties.
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // The LayoutInflator puts a layout into the right View



        ProfileHolder holder = null;
        if(convertView == null)
        {
            convertView = mInflater.inflate(R.layout.row_layout, null);

            holder = new ProfileHolder();

            holder.name = (TextView)convertView.findViewById(R.id.nameTextView);
            holder.cost = (TextView)convertView.findViewById(R.id.costTextView);
            holder.radioSelect = (RadioButton) convertView.findViewById(R.id.radioButton);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ProfileHolder) convertView.getTag();
        }

        holder.name.setText(getItem(position));

        holder.radioSelect.setChecked(position == selectedPosition);
        holder.radioSelect.setTag(position);
        holder.radioSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = (Integer)view.getTag();
                notifyDataSetChanged();
                Toast.makeText(getContext(), getItem(position) + "is Selected", Toast.LENGTH_SHORT).show();
            }
        });



        return convertView;

    }



    public static class ProfileHolder{
        TextView name;
        TextView cost;
        RadioButton radioSelect;

    }




}


