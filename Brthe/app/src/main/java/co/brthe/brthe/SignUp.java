package co.brthe.brthe;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.ParseUser;
import com.parse.SignUpCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by akhil on 10/3/15.
 */
public class SignUp extends Activity implements View.OnClickListener {

    private EditText mUserNameEditText;
    private EditText mEmailEditText;
    private EditText mPasswordEditText;
    private EditText mConfirmPasswordEditText;
    private EditText mAddressEditText;
    private EditText mPhoneNumberEditText;
    private Button mCreateAccountButton;
    private AutoCompleteTextView actv;


    private String mEmail;
    private String mUsername;
    private String mPassword;
    private String mConfirmPassword;
    private String mPhoneNumber;
    private String mAddress;
    private String mActv;
    private Filter filter;
    ArrayList<String> suggestion = new ArrayList<String>();;
    String[] locationSuges;

    ArrayAdapter<String> adapter;


    // flag for Internet connection status
    Boolean isInternetPresent = false;
    // Connection detector class
    ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        // creating connection detector class instance
        cd = new ConnectionDetector(getApplicationContext());


        mUserNameEditText = (EditText) findViewById(R.id.etUsername);
        mEmailEditText = (EditText) findViewById(R.id.etEmail);
        mPasswordEditText = (EditText) findViewById(R.id.etPassword);
        mConfirmPasswordEditText = (EditText) findViewById(R.id.etPasswordConfirm);
        mAddressEditText = (EditText) findViewById(R.id.etAddress);
        mPhoneNumberEditText = (EditText) findViewById(R.id.etPhoneNumber);
        actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        if(suggestion == null){
            locationSuges = new String[0];

        }
        else {
            locationSuges = new String[suggestion.size()];
            locationSuges = suggestion.toArray(locationSuges);

        }


        filter = new Filter() {
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                Log.i("Filter",
                        "Filter:" + constraint + " thread: " + Thread.currentThread());
                //if (constraint != null && constraint.length() > 10) {
                    new JSONLoadTask().execute("https://www.foodpanda.in/location-suggestions-ajax?cityId=17&area=" + URLEncoder.encode(actv.getText().toString()));

                return null;
            }
        };

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,suggestion) {
            public android.widget.Filter getFilter() {
                return filter;
            }
        };


        //if(actv.getText().toString() != "")
        //new JSONLoadTask().execute("https://www.foodpanda.in/location-suggestions-ajax?cityId=17&area=" + actv.getText().toString());

        /*
        if(suggestion == null){
            locationSuges = new String[0];

        }
        else {
            locationSuges = new String[suggestion.size()];
            locationSuges = suggestion.toArray(locationSuges);

        }*/
        actv.setThreshold(1);



        actv.setAdapter(adapter);
        adapter.setNotifyOnChange(true);


        mCreateAccountButton = (Button) findViewById(R.id.btnCreateAccount);
        mCreateAccountButton.setOnClickListener(this);

        actv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Log.isLoggable("2", count);
                //Log.d("below2", s.toString());
                //new JSONLoadTask().execute("https://www.foodpanda.in/location-suggestions-ajax?cityId=17&area=" + URLEncoder.encode(actv.getText().toString()));
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Log.d("1", "1");
                //new JSONLoadTask().execute("https://www.foodpanda.in/location-suggestions-ajax?cityId=17&area=" + URLEncoder.encode(actv.getText().toString()));



            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("3", "3");
                Log.d("below2", s.toString());
                new JSONLoadTask().execute("https://www.foodpanda.in/location-suggestions-ajax?cityId=17&area=" + URLEncoder.encode(actv.getText().toString()));



            }
        });




    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.btnCreateAccount:
                // get Internet status
                isInternetPresent = cd.isConnectingToInternet();
                // check for Internet status
                if (isInternetPresent) {
                    // Internet Connection is Present
                    // make HTTP requests
                    createAccount();
                } else {
                    // Internet connection is not present
                    // Ask user to connect to Internet
                    showAlertDialog(SignUp.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }


                break;

            default:
                break;
        }
    }

    private void createAccount(){
        clearErrors();

        boolean cancel = false;
        View focusView = null;

        // Store values at the time of the login attempt.
        mEmail = mEmailEditText.getText().toString();
        mUsername = mUserNameEditText.getText().toString();
        mPassword = mPasswordEditText.getText().toString();
        mConfirmPassword = mConfirmPasswordEditText.getText().toString();
        mAddress = mAddressEditText.getText().toString();
        mActv = actv.getText().toString();
        mPhoneNumber = mPhoneNumberEditText.getText().toString();


        if(TextUtils.isEmpty(mActv)){
            actv.setError(getString(R.string.error_field_required));
            focusView = actv;
            cancel = true;
        }
        // Check for a valid confirm password.
        if (TextUtils.isEmpty(mConfirmPassword)) {
            mConfirmPasswordEditText.setError(getString(R.string.error_field_required));
            focusView = mConfirmPasswordEditText;
            cancel = true;
        } else if (mPassword != null && !mConfirmPassword.equals(mPassword)) {
            mPasswordEditText.setError(getString(R.string.error_invalid_confirm_password));
            focusView = mPasswordEditText;
            cancel = true;
        }
        // Check for a valid password.
        if (TextUtils.isEmpty(mPassword)) {
            mPasswordEditText.setError(getString(R.string.error_field_required));
            focusView = mPasswordEditText;
            cancel = true;
        } else if (mPassword.length() < 4) {
            mPasswordEditText.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordEditText;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(mEmail)) {
            mEmailEditText.setError(getString(R.string.error_field_required));
            focusView = mEmailEditText;
            cancel = true;
        } else if (!mEmail.contains("@")) {
            mEmailEditText.setError(getString(R.string.error_invalid_email));
            focusView = mEmailEditText;
            cancel = true;
        }

        if (TextUtils.isEmpty(mAddress)) {
            mAddressEditText.setError(getString(R.string.error_field_required));
            focusView = mAddressEditText;
            cancel = true;
        }

        if (TextUtils.isEmpty(mPhoneNumber)) {
            mPhoneNumberEditText.setError(getString(R.string.error_field_required));
            focusView = mPasswordEditText;
            cancel = true;
        }
        else if(mPhoneNumber.length() != 10) {
            mPhoneNumberEditText.setError("Invalid PhoneNumber");
            focusView = mPasswordEditText;
            cancel = true;
        }

        if(TextUtils.isEmpty(mUsername)){
            mUserNameEditText.setError(getString(R.string.error_field_required));
            focusView = mUserNameEditText;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            Toast.makeText(this.getApplicationContext(), "signUp", Toast.LENGTH_SHORT).show();
            signUp(mUsername.toLowerCase(Locale.getDefault()), mEmail, mPassword);

        }

    }

    private void signUp(final String mUsername, String mEmail, String mPassword) {
        // TODO Auto-generated method stub
        Toast.makeText(getApplicationContext(), mUsername + " - " + mEmail, Toast.LENGTH_SHORT).show();
        ParseUser user = new ParseUser();
        user.setUsername(mUsername);
        user.setPassword(mPassword);
        user.setEmail(mEmail);
        user.put("location", mActv);
        user.put("address", mAddress);
        user.put("phoneNumber", mPhoneNumber);

        user.signUpInBackground(new SignUpCallback() {
            public void done(com.parse.ParseException e) {
                if (e == null) {
                    signUpMsg("Account Created Successfully");
                    Intent in = new Intent(SignUp.this,MainActivity.class);
                    startActivity(in);
                } else {
                    // Sign up didn't succeed. Look at the ParseException
                    // to figure out what went wrong
                    signUpMsg("Account already taken.");
                }
            }

        });
    }

    protected void signUpMsg(String msg) {
        // TODO Auto-generated method stub
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    private void clearErrors(){
        mEmailEditText.setError(null);
        mUserNameEditText.setError(null);
        mPasswordEditText.setError(null);
        mConfirmPasswordEditText.setError(null);
        mAddressEditText.setError(null);

    }

    @SuppressWarnings("deprecation")
    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon(R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }




    public class JSONLoadTask extends AsyncTask<String, Void, JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //your code
        }
        @Override
        protected JSONObject doInBackground(String... params) {
            JSONParser jParser = new JSONParser();
            JSONObject object = jParser.getJsonObject(params[0]);
            return object;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            JSONArray jArray = null;

            try {
                jArray = result.getJSONArray("data");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            try {
                int length = jArray.length();
                if(length > 5){
                    length = 5;
                }
                if(length != 0) {
                    int i;
                   suggestion = new ArrayList<String>();

                    for (i = 0; i < length; i++) {
                        suggestion.add(jArray.getJSONObject(i).getString("value"));
                    }
                }

                String[] locationSuges;

                if(suggestion == null){
                    locationSuges = new String[0];

                }
                else {
                    locationSuges = new String[suggestion.size()];
                    locationSuges = suggestion.toArray(locationSuges);

                }

                adapter.clear();
                int i = 0;
                if (locationSuges != null){
                    Log.d("I'm here", locationSuges.toString());

                    for (String st : locationSuges) {

                        adapter.insert(st, i);
                        Log.d("string", st);
                        i = i + 1;
                        if(i == 5){
                            break;
                        }
                    }
                }
                Log.d("changes", "here");
                // adapter[0] = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,locationSuges);
                adapter.notifyDataSetChanged();
                actv.showDropDown();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            //get the values from result here
        }

    }


}
